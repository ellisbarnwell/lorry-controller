WEBAPP status reporting
=======================

WEBAPP reports it status via an HTTP request. We verify that when it
starts up, the status is that it is doing nothing: there are no jobs,
it has no Lorry or Host specs.

    SCENARIO WEBAPP is idle when it starts
    GIVEN a running WEBAPP
    WHEN admin makes request GET /1.0/status
    THEN response is application/json
    AND response has running_queue set to true
    AND response has disk_free set
    AND response has disk_free_mib set
    AND response has disk_free_gib set
    AND static status page got updated
    FINALLY WEBAPP terminates

As an alternative, we can request the HTML rendering of the status
directly with `/1.0/status-html`.

    SCENARIO WEBAPP provide HTML status directly
    GIVEN a running WEBAPP
    WHEN admin makes request GET /1.0/status-html
    THEN response is text/html
    AND static status page got updated
    FINALLY WEBAPP terminates
