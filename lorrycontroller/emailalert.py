# Copyright (C) 2021  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import logging

import bottle
import smtplib
import platform

import lorrycontroller

class SendEmail(lorrycontroller.LorryControllerRoute):

    http_method = 'POST'
    path = '/1.0/send-alerts'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)

        statedb = self.open_statedb()
        with statedb:
            paths = statedb.get_lorries_paths()

            for lorry_path in paths:
                in_failure_state, in_success_state = \
                    self.get_failure_n_success_state(statedb, lorry_path)
                logging.debug('Checked failure/success state for lorry %r: '
                    'in_failure_state = %r, in_success_state = %r',
                    lorry_path, in_failure_state, in_success_state)

                if statedb.get_email_blocked(lorry_path):
                    if in_success_state:
                        logging.debug('Entered success state for lorry %r, '
                            'unblocking future e-mails', (lorry_path))
                        statedb.set_email_blocked(lorry_path, 0)
                else:
                    if in_failure_state:
                        self.send_email_alert(statedb, lorry_path)
                        statedb.set_email_blocked(lorry_path, 1)

        if 'redirect' in bottle.request.forms:
            bottle.redirect(bottle.request.forms.redirect)

        return 'Failure status has been checked and e-mails sent.'

    # Return a tuple of (in_failure_state, in_success_state)
    # failure_alert_threshold failures in a row are required to be in the
    # failure state.
    # Success state is based on the exit status of the last job.
    # Note, we could be in an indeterminate state if there are some failures
    # and some successes.
    def get_failure_n_success_state(self, statedb, lorry_path):
        failure_alert_threshold = self.app_settings['failure-alert-threshold']
        logging.debug('Checking if in failure state, number of fails required ='
            + str(failure_alert_threshold))

        last_n_jobs_for_lorry = statedb.get_last_n_jobs_for_lorry(lorry_path, 
            failure_alert_threshold)

        if ((len(last_n_jobs_for_lorry) < failure_alert_threshold) or
            (failure_alert_threshold < 1)):
            return (False, False)

        # Consider success state if last job succeeded
        last_job_info = statedb.get_job_info(last_n_jobs_for_lorry[-1])
        if int(last_job_info['exit']) == 0:
            in_success_state = True
        else:
            in_success_state = False

        # Assume true until proven false
        # In failure state if we find one success
        in_failure_state = True

        for job in last_n_jobs_for_lorry:
            job_info = statedb.get_job_info(job)
            last_run_exit = int(job_info['exit'])
            if last_run_exit == 0:
                in_failure_state = False

        return (in_failure_state, in_success_state)

    def send_email_alert(self, statedb, lorry_path):
        email_to = statedb.get_owner_email(lorry_path)
        email_from = self.app_settings['alert_sent_from']
        smtp_server = self.app_settings['smtp_server']
        smtp_user = self.app_settings['smtp_user']
        smtp_pass = self.app_settings['smtp_pass']

        email_body = self.get_email_body_from_statedb(lorry_path, statedb)

        if smtp_server == '':
            logging.warning('SMTP server not set. Cannot send e-mails. Add '
                'smtp_server to webapp config file.')
            return

        logging.debug('Sending e-mail alert for lorry %r to e-mail address %r',
            lorry_path, email_to)

        try:
            smtp_server = smtplib.SMTP_SSL(smtp_server, 465)
            smtp_server.login(smtp_user, smtp_pass)
            smtp_server.sendmail(email_from, email_to, email_body)
            smtp_server.close()
            logging.debug("Email sent successfully!")
        except Exception as ex:
            logging.warning("Problem sending e-mail alert using SMTP server %r."
                , smtp_server)
            logging.warning(ex)

    def get_email_body_from_statedb(self, lorry_path, statedb):

        failure_alert_threshold = self.app_settings['failure-alert-threshold']
        last_n_jobs_for_lorry = statedb.get_last_n_jobs_for_lorry(lorry_path, 
            failure_alert_threshold)

        job_exit = {}
        for job in last_n_jobs_for_lorry:
            job_exit[job] = statedb.get_job_exit(job)

        lorry_info = statedb.get_lorry_info(lorry_path)

        n_lines = 50
        last_job_output = statedb.get_job_output(last_n_jobs_for_lorry[-1])
        last_job_output = self.truncate_to_last_n_lines(last_job_output, 
            n_lines)

        machine_hostname = platform.node()

        email_body = self.get_email_body(lorry_path, lorry_info['text'], 
            job_exit, last_job_output, machine_hostname)

        return email_body

    def get_email_body(self, lorry_path, lorry_info, job_exit, last_job_output, 
        machine_hostname):
        email_body = ("Subject: lorry-controller: Error occured in fetching "
            "lorry mirror.\n\n"
            "This is an autogenerated alert from lorry-controller running on "
            " machine %s.\n\n"
            "Error occurred for lorry with path %s. Lorry info:\n\n"
            "%s\n\n"
            "Exit statuses for most recent jobs:\n\n"
            "Job_ID: Exit status\n"
            + self.get_exit_status_string(job_exit)
            + "\n\n"
            "Sample of output from most recent job:\n\n...\n"
            "%s"
            ) % (machine_hostname, lorry_path, lorry_info, last_job_output)

        return email_body

    def get_exit_status_string(self, job_exit):
        exit_status_string = ""
        for key, value in job_exit.items():
            exit_status_string += "%s: %s\n" % (key, value)
        return exit_status_string

    def truncate_to_last_n_lines(self, string, n_lines):
        last_occurance = string.rfind("\n")
        for i in range(0, n_lines -1):
            last_occurance = string.rfind("\n", 0, last_occurance)
        return string[last_occurance+1:]
