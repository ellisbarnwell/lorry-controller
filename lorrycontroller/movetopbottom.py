# Copyright (C) 2014  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import logging

import bottle

import lorrycontroller


class MoveBase(object):

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)
        path = bottle.request.forms.path
        if not path:
            return 'Form field path was not given'
        statedb = self.open_statedb()
        with statedb:
            if not self.lorry_exists(statedb, path):
                return 'Lorry %s does not exist' % path
            lorry_infos = statedb.get_all_lorries_info()
            timestamp = self.get_new_timestamp(lorry_infos)
            statedb.set_lorry_last_run(path, timestamp)
            return self.msg % path

    def lorry_exists(self, statedb, path):
        try:
            statedb.get_lorry_info(path)
        except lorrycontroller.LorryNotFoundError:
            return False
        else:
            return True


class MoveToTop(MoveBase, lorrycontroller.LorryControllerRoute):

    http_method = 'POST'
    path = '/1.0/move-to-top'
    msg = 'Lorry %s moved to the top of the run queue'

    def get_new_timestamp(self, lorry_infos):
        topmost = lorry_infos[0]
        timestamp = min(0, topmost['last_run'] - 1)
        return timestamp


class MoveToBottom(MoveBase, lorrycontroller.LorryControllerRoute):

    http_method = 'POST'
    path = '/1.0/move-to-bottom'
    msg = 'Lorry %s moved to the bottom of the run queue'

    def get_new_timestamp(self, lorry_infos):
        bottommost = lorry_infos[-1]
        timestamp = bottommost['last_run'] + bottommost['interval'] + 1
        return timestamp
