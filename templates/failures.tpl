<!DOCTYPE HTML>
<html>
    <head>
        <title>Failing lorries</title>
        <link rel="stylesheet" href="/lc-static/style.css" type="text/css" />
    </head>
    <body>
        <h1>Failing lorries</h1>

        <p>See the <a href="/1.0/status-html">full status</a>.</p>

        <table>
            <tr>
                <th>Path</th>
                <th>Due</th>
                <th>Last run exit</th>
                <th>Job?</th>
            </tr>
%           failures = [spec for spec in run_queue
%                       if spec['last_run_exit'] not in [None, '0']]
%           for spec in sorted(failures, key=(lambda spec: spec['path'])):
                <tr>
                    <td><a href="/1.0/lorry-html/{{spec['path']}}">{{spec['path']}}</a></td>
                    <td nowrap>{{spec['due_nice']}}</td>
                    <td>
                        <details>
                            <summary>{{spec['last_run_exit']}}: Show log</summary>
                            <p><pre>{{spec['last_run_error']}}</pre></p>
                        </details>
                    </td>
%                   if spec['running_job']:
                        <td><a href="/1.0/job-html/{{spec['running_job']}}">{{spec['running_job']}}</a></td>
%                   else:
                        <td></td>
%                   end
                </tr>
%           end
        </table>

        <hr>

        <p>Updated: {{timestamp}}</p>

    </body>
</html>
