<!DOCTYPE HTML>
<html>
    <head>
        <title>Lorry {{lorry['path']}}</title>
        <link rel="stylesheet" href="/lc-static/style.css" type="text/css" />
    </head>
    <body>
        % import json

        <h1>Lorry {{lorry['path']}}</h1>

<table>

<tr> <th>Path</th> 
  <td><a href="{{http_server_root}}/cgi-bin/cgit.cgi/{{lorry['path']}}.git/">{{lorry['path']}}</a></td> </tr>
<tr> <th>URL</th> <td>{{lorry['url']}}</td> </tr>
<tr> <th>Interval</th> <td>{{lorry['interval_nice']}} ({{lorry['interval']}} s)</td> </tr>
<tr> <th>Last run</th> <td>{{lorry['last_run_nice']}}</td> </tr>
<tr> <th>Due</th> <td>{{lorry['due_nice']}}</td> </tr>
<tr> <th>From Host</th> <td>{{lorry['from_host']}}</td> </tr>
<tr> <th>Disk usage</th> <td>{{lorry['disk_usage_nice']}}</td> </tr>

<tr> <th>Job?</th>
%   if lorry['running_job']:
<td><a href="/1.0/job/{{lorry['running_job']}}">{{lorry['running_job']}}</a></td>
%   else:
<td></td>
%   end
</tr>

</table>

<h2>Full Lorry text</h2>

<blockquote>
<pre>{{lorry['text']}}</pre>
</blockquote>

        <hr />

        <p>Updated: {{timestamp}}</p>

<h2>Failed jobs for this Lorry</h2>

% if lorry['failed_jobs']:
<p>
%   for job_id in lorry['failed_jobs']:
<a href="/1.0/job-html/{{job_id}}">{{job_id}}</a>
%   end
</p>
% else:
<p>No failed jobs.</p>
% end

<h2>All jobs for this lorry</h2>

% if lorry['jobs']:
<p>
%   for job_id in lorry['jobs']:
<a href="/1.0/job-html/{{job_id}}">{{job_id}}</a>
%   end
</p>
% else:
<p>No jobs.</p>
% end


    </body>
</html>
